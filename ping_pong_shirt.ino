//Pong Shirt v0.1 alpha

#include <SoftwareSerial.h>
#include <Wire.h>
//#include "Adafruit_LEDBackpack.h"
//#include "Adafruit_GFX.h"

// for wiring SEE PWIRING http://arduino.cc/forum/index.php/topic,19485.0.html  it's pins 10++


// Simple program to test using the Arduino with the RGB Matrix
// & Backpack from Sparkfun. Code is a combination of Heather Dewey-Hagborg,
// Arduino Forum user: Little-Scale, and // Daniel Hirschmann. Enjoy!
//
// The Backpack requires 125Khz SPI, which is the slowest rate
// at which the Arduino's hardware SPI bus can communicate at.
//
// We need to send SPI to the backpack in the following steps:
// 1) Activate ChipSelect;
// 2) Wait 500microseconds;
// 3) Transfer 64bytes @ 125KHz (1 byte for each RGB LED in the matrix);
// 4) De-activate ChipSelect;
// 5) Wait 500microseconds
// Repeat however often you like!

boolean serving = true;

int matrixWidth = 8;
int matrixHeight = 8;

boolean resetState = false;
int flashCounter = 0;
int player1Score = 0;
int player2Score = 0;

//Colour to flash upon scoring
int currentScoreColour = 0;

// FROM EXAMPLE CODE
#define CHIPSELECT 10//ss
#define SPICLOCK  13//sck
#define DATAOUT 11//MOSI / DI
#define DATAIN 12//MISO / DO

const int resetPin1 = 0;
const int resetPin2 = 2;

int resetPressed = 0; //TODO: remove

boolean waitMode = true;


//Paddle Input
int sensorValue;
int sensorValue2;

int sensorPin = A0;    // select the input pin for the potentiometer
int paddlePin2 = A1;    // select the input pin for the potentiometer

//Paddle Colour
int paddle1Colour = 2; // sets to red
int paddle2Colour = 1;  // sets to green

//Paddle Position
int paddle1Positions[2]; // set the initial screen array vals for paddle 1
int paddle2Positions[2]; // set the initial screen array vals for paddle 2


//Ball
int ballColour = 3; // sets to orange

int ballXVel = 0;
int ballYVel = 1;
int ballXPos;
int ballYPos;

int coord; //ball position in linear format

//---

int data[] =  // this is the screen array -- this writes every iteration thru loop -- by altering the values we changthe vals of the 
//rray index positions 
{
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0,
  0,0,0,0,0,0,0,0
};


// this was from the SPI comms example
char spi_transfer(volatile char data)
{
  SPDR = data;                    // Start the transmission
  while (!(SPSR & (1<<SPIF)))     // Wait the end of the transmission
  {
  };
}// this was from the SPI comms example


void setup()
{
  Serial.begin(9600);

  pinMode(resetPin1, INPUT);
  pinMode(resetPin2, INPUT);
  
  digitalWrite(resetPin1, HIGH);
  digitalWrite(resetPin2, HIGH);

  sensorValue = analogRead(sensorPin);
  sensorValue2 = analogRead(paddlePin2);
  sensorValue = map(sensorValue, 0, 1023, 0, 7);
  sensorValue2 = map(sensorValue2, 0, 1023, 0, 7);

  paddle1Positions[0] = sensorValue;
  paddle1Positions[1] = paddle1Positions[0] + 1;

  paddle2Positions[0] = 63 - sensorValue2;
  paddle2Positions[1] = paddle2Positions[0] + 1;

  snapBallToPaddle();

  byte clr; // this was from the SPI comms example
  pinMode(DATAOUT,OUTPUT); // this was from the SPI comms example
  pinMode(SPICLOCK,OUTPUT); // this was from the SPI comms example
  pinMode(CHIPSELECT,OUTPUT); // this was from the SPI comms example
  digitalWrite(CHIPSELECT,HIGH); //disable device // this was from the SPI comms example

  SPCR = B01010001;             //SPI Registers // this was from the SPI comms example
  SPSR = SPSR & B11111110;      //make sure the speed is 125KHz  // this was from the SPI comms example

  /*
   SPCR bits:
   7: SPIEE - enables SPI interrupt when high
   6: SPE - enable SPI bus when high
   5: DORD - LSB first when high, MSB first when low
   4: MSTR - arduino is in master mode when high, slave when low
   3: CPOL - data clock idle when high if 1, idle when low if 0
   2: CPHA - data on falling edge of clock when high, rising edge when low
   1: SPR1 - set speed of SPI bus
   0: SPR0 - set speed of SPI bus (00 is fastest @ 4MHz, 11 is slowest @ 250KHz)
   */

  clr=SPSR; // this was from the SPI comms example
  clr=SPDR; // this was from the SPI comms example
  delay(10); // this was from the SPI comms example

}  // end setup




void loop()       
{
//  Serial.print("X pos = ");
//  Serial.print(ballXPos);
//  Serial.print(" , Y pos = ");
//  Serial.println(ballYPos);
  //Clear screen
  clrMatrix();

  //Check if reset buttons pressed
  //  Serial.println(digitalRead(resetPin1));
//    Serial.print("Switch-2 is: ");
//    Serial.println(digitalRead(resetPin2));
//    Serial.print("Switch is: ");
//    Serial.println(digitalRead(resetPin1));

  if (waitMode == true) {
    updatePaddlePosition();

    //Display ball
    snapBallToPaddle();
    //coord = ballXPos + ballYPos * 8;
    //    ballYPos = (coord - ballXPos)/8;
    //    ballXPos = coord - (ballYPos * 8);
    data[coord] = ballColour;

    //Check for wait escape
    
    if (currentScoreColour == paddle1Colour) {
      if (digitalRead(resetPin1) == 0) waitMode = false;
    } else if (currentScoreColour == paddle2Colour) {
      if (digitalRead(resetPin2) == 0) waitMode = false;
    }
  }
  else if (resetState == true) {
    reset();
  } 
  else {
    updatePaddlePosition();
    assignAngle();
    updateBallPos();

    //Convert ball coordinates into linear array position
    coord = ballXPos + ballYPos * 8;

    data[coord] = ballColour;
  }

  //////////////////////////////////////////////////////////////
  // THIS SENDS OUT THE ARRAY TO THE LED MATRIX//////////////// 
  delay(300);  //// this affescts the speed of the ball bouncing game 
  int index = 0;                  
  digitalWrite(CHIPSELECT,LOW); // enable the ChipSelect on the backpack
  delayMicroseconds(500);
  for (int i=0;i<8;i++) 
    for (int j=0;j<8;j++)
    {
      spi_transfer(data[index]);
      index++;      
    }
  digitalWrite(CHIPSELECT,HIGH); // disable the ChipSelect on the backpack
  delayMicroseconds(500);
  // END OF SENDING TO LED MATRIX 
  //////////////////////////////////////////////////////////////
  //////////////////////////////////////////////////////////////

} //END LOOP()

void updatePaddlePosition() {

  int prevVal = sensorValue;
  int prevVal2 = sensorValue2;

  sensorValue = analogRead(sensorPin);  // read pin //paddle 1
  sensorValue2 = analogRead(paddlePin2); // paddle 2
  
  

  int delta = sensorValue - prevVal;
  int delta2 = sensorValue2 - prevVal2;
  
  Serial.print("Delta is");
  Serial.println(delta);
  
  Serial.print("Delta 2 is ");
  Serial.println(delta2);

  int padVel = map (abs(delta), 0, 250, 1, 3);
  int pad2Vel = map (abs(delta2), 0, 250, 1, 3);

  if (delta < 0) padVel = padVel * -1;
  if (delta2 < 0) pad2Vel = pad2Vel * -1;
  if (delta < 5 && delta > -5) padVel = 0;
  if (delta2 < 5 && delta2 >-5) pad2Vel = 0;

  //  int mappedValue = map(sensorValue, 0.0, 1023, 0, 7); 
    //  int mappedValue2 = map(sensorValue2, 0.0, 1023, 0,7);  

    //Serial.println(pad2Vel);

  //    Serial.println(sensorValue2);

  //Paddle 1 position assignment
  paddle1Positions[0] = constrain(paddle1Positions[0]+ padVel, 0, 6);//mappedValue;   // the map func above constrained to the first row, i.e. index 0-7
  paddle1Positions[1] = paddle1Positions[0] + 1;//mappedValue+1;

  //Paddle 2 position assignment
  paddle2Positions[0] = constrain(paddle2Positions[0] + pad2Vel, 56, 62);//mappedValue2; // the map func above constrained to the last row, i.e. index 63-64
  paddle2Positions[1] = paddle2Positions[0] + 1;//mappedValue2+1;


  //Paddle 1
  data[paddle1Positions[0]] = paddle1Colour;  // set color
  data[paddle1Positions[1]] = paddle1Colour;  // set color

    //Paddle 2
  data[paddle2Positions[0]] = paddle2Colour;  // set color
  data[paddle2Positions[1]] = paddle2Colour;  // set color
}

void updateBallPos () {
  ballXPos += ballXVel;
  ballYPos += ballYVel;
}

void clrMatrix() {
  for (int i = 0; i < 64; i++) {
    data[i] = 0;
  }
}

int randomVel() {
  int randVel = random(-1, 2);
  return randVel;
}

void reset() {
  int numFlashes = 5;

  //Flash for 5
  if (flashCounter/2  <=  numFlashes) {
    for (int a = 0; a < 64; a++) {
      if (flashCounter % 2 == 0) {
        data[a] = currentScoreColour;
      } 
      else {
        data[a] = 0;
      }
    }
    flashCounter++;
  }
  //Show score for 5
  else if (flashCounter/2 < 10 && flashCounter/2 > numFlashes){
    showScore();
    flashCounter++;
  }
  else {
    //Set Game Up
    snapBallToPaddle();
    flashCounter = 0;
    resetState = false;
    waitMode = true;
    serving = true;
  }
}


//Puts the ball against the paddle of the last scoring player
void snapBallToPaddle() {
  if (currentScoreColour == paddle1Colour) { 
    coord = paddle1Positions[0] + matrixWidth;
    ballYPos = 1;
  } 
  else if (currentScoreColour == paddle2Colour) {
    coord = paddle2Positions[0] - matrixWidth;
    ballYPos = 6;
  } 
  else {
    int generateSnap = round(random(0, 2));
    if (generateSnap == 0) {
      coord = paddle1Positions[0] + matrixWidth; 
      ballYPos = 1;
      currentScoreColour = paddle1Colour;
    } 
    else {
      coord = paddle2Positions[0] - matrixWidth;
      ballYPos = 6;
      currentScoreColour = paddle2Colour;
    }
  }

  ballXVel = 0;
  //ballYVel = -1 * ballYVel;
  //ballYPos = (coord - ballXPos)/8;
  ballXPos = coord - (ballYPos * 8);
}
void showScore() {
  clrMatrix();
  // for (int i = 0; i < matrixWidth; i++) {
  //  data[i-(matrixHeight/2*matrixWidth)] = 2; 
  //  data[i-((matrixHeight/2-1)*matrixWidth)] = 2; 
  // }

  //Show player 1 score
  for (int i = 0; i < player1Score; i++) {
    data[7-i] = paddle1Colour;
    data[7-i+(1*matrixWidth)] = paddle1Colour;
  }
  //Show player 2 score
  for (int i = 0; i < player2Score; i++) {
    data[7-i+((matrixHeight-1)*matrixWidth)] = paddle2Colour;
    data[7-i+((matrixHeight-2)*matrixWidth)] = paddle2Colour;
  }

  if (player1Score == 8 || player2Score == 8) {
    player1Score = 0;
    player2Score = 0; 
  }
}

//Jon's Additions
void assignAngle () {

  //we know paddle positions in raw format
  //we know ball position in x, y format
  //we want to work in cartesian
  //Define Temp Cartesian Paddle Positions

  //Top Paddle
  int padX = paddle1Positions[0];

  //Top Paddle2
  int pad2X = paddle2Positions[0] - (7 * 8);
  //Serial.println(pad2X);  
  //if touching top paddle
  //random between reverse and straight bounce

  //if touching left or right wall
  //reverse velocity
  if (ballXPos == 0) {
    ballXVel = 1;
  } 
  else if (ballXPos == 7) {
    ballXVel = -1;
  }

  //If hitting top paddle
  if (ballYPos == 1 && ballXPos + ballXVel == padX || 
    ballYPos == 1 && ballXPos + ballXVel == padX+1) {
    ballXVel = randomVel();
    ballYVel = 1;
  }
  //If hitting bottom paddle
  else if (ballYPos == 6 && ballXPos + ballXVel == pad2X ||
    ballYPos == 6 && ballXPos + ballXVel == pad2X+1) {
    ballXVel = randomVel();
    ballYVel = -1;
  }
  //Player 2 scored 
  else if (ballYPos == 0) {
    resetState = true;
    player2Score++;
    currentScoreColour = paddle2Colour;
  }
  //Player 1 scored
  else if (ballYPos == 7) {
    resetState = true;
    player1Score++;
    currentScoreColour = paddle1Colour;
  }

  if (serving == true) {
    ballXVel = 0;
    serving = false;
  }
}













